<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/register', 'AuthController@register');
$router->post('/login', 'AuthController@login');

$router->get('/checklist', 'ChecklistController@list');
$router->post('/checklist', 'ChecklistController@store');
$router->delete('/checklist/{checklistId}', 'ChecklistController@destroy');

$router->get('/checklist/{checklistId}/item', 'ChecklistItemController@listByChecklistId');
$router->post('/checklist/{checklistId}/item', 'ChecklistItemController@store');
$router->get('/checklist/{checklistId}/item/{checklistItemId}', 'ChecklistItemController@getOne');
$router->put('/checklist/{checklistId}/item/{checklistItemId}', 'ChecklistItemController@updateStatus');
$router->delete('/checklist/{checklistId}/item/{checklistItemId}', 'ChecklistItemController@destroy');
$router->put('/checklist/{checklistId}/item/rename/{checklistItemId}', 'ChecklistItemController@rename');
