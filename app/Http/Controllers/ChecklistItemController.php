<?php

namespace App\Http\Controllers;

use App\Http\Resources\ChecklistItemResource;
use App\Http\Resources\ChecklistResource;
use App\Models\Checklist;
use App\Models\ChecklistItem;
use Illuminate\Http\Request;

class ChecklistItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function listByChecklistId($checklistId)
    {
        $models = ChecklistItem::where('checklist_id', $checklistId)->get();
        return ChecklistItemResource::collection($models);
    }

    public function store(Request $request, $checklistId)
    {
        $this->validate($request, [
            'itemName' => 'required|string|max:255'
        ]);

        if (!$checklist = Checklist::find($checklistId)) {
            return response()->json(['message' => 'Data checklist not found'], 404);
        }

        $model = ChecklistItem::create(['checklist_id'=>$checklist->id, 'item_name'=>$request->post('itemName'), 'status'=>0]);
        return new ChecklistItemResource($model);
    }

    public function getOne($checklistId, $checklistItemId)
    {
        $model = ChecklistItem::where('checklist_id', $checklistId)->where('id', $checklistItemId)->first();

        return ChecklistItemResource::make($model);
    }

    public function updateStatus($checklistId, $checklistItemId)
    {
        if (!$checklist = Checklist::find($checklistId)) {
            return response()->json(['message' => 'Data checklist not found'], 404);
        }
        if (!$checklistItem = ChecklistItem::find($checklistItemId)) {
            return response()->json(['message' => 'Data checklist item not found'], 404);
        }
        $checklistItem->status = 1;
        $checklistItem->save();

        return ChecklistItemResource::make($checklistItem);
    }

    public function destroy($checklistId, $checklistItemId)
    {
        if (!$checklist = Checklist::find($checklistId)) {
            return response()->json(['message' => 'Data checklist not found'], 404);
        }
        if (!$checklistItem = ChecklistItem::find($checklistItemId)) {
            return response()->json(['message' => 'Data checklist item not found'], 404);
        }

        $checklistItem->delete();
        return response()->json(['message'=>'Data deleted successfuly'], 200);
    }

    public function rename(Request $request, $checklistId, $checklistItemId)
    {
        if (!$checklist = Checklist::find($checklistId)) {
            return response()->json(['message' => 'Data checklist not found'], 404);
        }
        if (!$checklistItem = ChecklistItem::find($checklistItemId)) {
            return response()->json(['message' => 'Data checklist item not found'], 404);
        }

        $this->validate($request, [
            'itemName' => 'required|string|max:255'
        ]);

        $checklistItem->item_name = $request->post('itemName');
        $checklistItem->save();

        return ChecklistItemResource::make($checklistItem);
    }
}
