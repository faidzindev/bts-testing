<?php

namespace App\Http\Controllers;

use App\Http\Resources\ChecklistResource;
use App\Models\Checklist;
use Illuminate\Http\Request;

class ChecklistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function list()
    {
        $models = Checklist::all();

        return ChecklistResource::collection($models);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255'
        ]);

        $model = Checklist::create($request->only(['name']));
        return new ChecklistResource($model);
    }

    public function destroy($checklistId)
    {
        if (!$model = Checklist::find($checklistId)) {
            return response()->json(['message' => 'Data not found'], 404);
        }

        $model->delete();
        return response()->json(['message'=>'Data deleted successfuly'], 200);
    }
}
